import React from 'react';

const SVGComponent = () => (
    <svg width="21" height="21" viewBox="0 0 21 21" xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink">
        <defs>
            <path d="M10.5,20.2222222 C5.13056493,20.2222222 0.777777778,15.8694351 0.777777778,10.5 C0.777777778,5.13056493 5.13056493,0.777777778 10.5,0.777777778 C15.8694351,0.777777778 20.2222222,5.13056493 20.2222222,10.5 C20.2222222,15.8694351 15.8694351,20.2222222 10.5,20.2222222 Z M13.7220832,7.38197952 L8.57621475,12.5278479 L6.81246493,10.7640981 C6.43278853,10.3844217 5.81721147,10.3844217 5.43753507,10.7640981 C5.05785868,11.1437745 5.05785868,11.7593516 5.43753507,12.139028 L7.88874983,14.5902427 C8.26842622,14.9699191 8.88400328,14.9699191 9.26367968,14.5902427 L15.097013,8.75690937 C15.4766894,8.37723298 15.4766894,7.76165591 15.097013,7.38197952 C14.7173366,7.00230312 14.1017596,7.00230312 13.7220832,7.38197952 Z"
                id="path-1" />
        </defs>
        <g id="Product-Flow" fill="none" fillRule="evenodd">
            <g id="Dashboard-New---Create-Job+Step2:-Application-form" transform="translate(-420 -123)">
                <g id="Progress-bar" transform="translate(365 112)">
                    <g id="Step1-Tile">
                        <g id="💚-Icon" transform="translate(55 11)">
                            <mask id="mask-2-completed-dashboard" fill="#fff">
                                <use xlinkHref="#path-1" />
                            </mask>
                            <use id="Mask" fillOpacity="0" fill="#051033" xlinkHref="#path-1" />
                            <g id="↳🎨-Icon-Color" mask="url(#mask-2-completed-dashboard)" fill="#4767A0">
                                <polygon points="0 0 23.3333333 0 23.3333333 23.3333333 0 23.3333333"
                                    transform="translate(-1.167 -1.167)" id="White" />
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export default SVGComponent;