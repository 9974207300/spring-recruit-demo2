import React from 'react';

const SVGComponent = () => (
    <svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
        <g id="Product-Flow" fill="none" fillRule="evenodd">
            <g id="Dashboard-New---Create-Job+Step3:-Promote" transform="translate(-1005 -786)"
                fillRule="nonzero">
                <g id="Candidate-Application" transform="translate(365 200)">
                    <g id="Group-2-Copy-2" transform="translate(0 508)">
                        <g id="Share-Options-Copy-3" transform="translate(625 60)">
                            <g id="Share-Option1">
                                <g id="Logo" transform="translate(15 18)">
                                    <path d="M21.4285714,0 L2.57142857,0 C1.15178571,0 0,1.11979167 0,2.5 L0,20.8333333 C0,22.2135417 1.15178571,23.3333333 2.57142857,23.3333333 L21.4285714,23.3333333 C22.8482143,23.3333333 24,22.2135417 24,20.8333333 L24,2.5 C24,1.11979167 22.8482143,0 21.4285714,0 Z"
                                        id="Path" fill="#4767A0" />
                                    <path d="M19.9499612,6.26173418 L17.384796,18.0229573 C17.1912702,18.8530311 16.6865854,19.0596272 15.9694017,18.6685702 L12.06094,15.8684547 L10.1750124,17.6319003 C9.9663081,17.8348072 9.79175544,18.0045112 9.38952541,18.0045112 L9.67032751,14.1345228 L16.9142628,7.77062385 C17.2292165,7.49762181 16.8459596,7.34636392 16.4247564,7.61936596 L7.46944618,13.1015421 L3.61410924,11.9283712 C2.77549756,11.6738152 2.76031907,11.1130543 3.78866189,10.7219973 L18.8684936,5.07380638 C19.5667042,4.81925042 20.1776385,5.22506427 19.9499612,6.26173418 Z"
                                        id="telegram-plane" fill="#FFF" />
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export default SVGComponent;