import React from 'react';

const SVGComponent = () => (
    <svg width="19" height="14" viewBox="0 0 19 14" xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink">
        <defs>
            <path d="M0,6.90909091 C1.6435,2.97001181 4.75,0 9.5,0 C14.25,0 17.3565,2.97001181 19,6.90909091 C17.3565,10.84817 14.25,13.8181818 9.5,13.8181818 C4.75,13.8181818 1.6435,10.84817 0,6.90909091 Z M6.90909091,6.90909091 C6.90909091,5.47364336 8.06455245,4.31818182 9.5,4.31818182 C10.9354476,4.31818182 12.0909091,5.47364336 12.0909091,6.90909091 C12.0909091,8.34453846 10.9354476,9.5 9.5,9.5 C8.06455245,9.5 6.90909091,8.34453846 6.90909091,6.90909091 Z M5.18181818,6.90909091 C5.18181818,9.29848485 7.11060606,11.2272727 9.5,11.2272727 C11.8893939,11.2272727 13.8181818,9.29848485 13.8181818,6.90909091 C13.8181818,4.51969697 11.8893939,2.59090909 9.5,2.59090909 C7.11060606,2.59090909 5.18181818,4.51969697 5.18181818,6.90909091 Z"
                id="path-1" />
        </defs>
        <g id="Product-Flow" fill="none" fillRule="evenodd">
            <g id="Dashboard-New---Create-Job+Step3:-Promote" transform="translate(-75 -216)">
                <g id="Add-Stage-Btn" transform="translate(40 200)">
                    <g id="Add-Candidate" transform="translate(35 12)">
                        <g id="💚-Icon" transform="translate(0 4)">
                            <mask id="mask-2-preview" fill="#fff">
                                <use xlinkHref="#path-1" />
                            </mask>
                            <use id="Mask" fill="#4767A0" xlinkHref="#path-1" />
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export default SVGComponent;