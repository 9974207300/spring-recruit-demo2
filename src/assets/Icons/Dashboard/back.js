import React from 'react';

const SVGComponent = () => (
    <svg width="18" height="18" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink">
        <defs>
            <path d="M4.7636039,9.6363961 L8.3636039,13.2363961 C8.71507576,13.587868 9.28492424,13.587868 9.6363961,13.2363961 C9.98786797,12.8849242 9.98786797,12.3150758 9.6363961,11.9636039 L7.57279221,9.9 L12.6,9.9 C13.0970563,9.9 13.5,9.49705627 13.5,9 C13.5,8.50294373 13.0970563,8.1 12.6,8.1 L7.57279221,8.1 L9.6363961,6.0363961 C9.98786797,5.68492424 9.98786797,5.11507576 9.6363961,4.7636039 C9.28492424,4.41213203 8.71507576,4.41213203 8.3636039,4.7636039 L4.7636039,8.3636039 C4.60073593,8.52647186 4.5,8.75147186 4.5,9 C4.5,9.24852814 4.60073593,9.47352814 4.7636039,9.6363961 Z M9,18 C4.02943725,18 0,13.9705627 0,9 C0,4.02943725 4.02943725,0 9,0 C13.9705627,0 18,4.02943725 18,9 C18,13.9705627 13.9705627,18 9,18 Z"
                id="path-1" />
        </defs>
        <g id="Product-Flow" fill="none" fillRule="evenodd">
            <g id="Dashboard-New---Create-Job+Step1:Job-details" transform="translate(-75 -213)">
                <g id="Side-Menu" transform="translate(40 200)">
                    <g id="Back-To-Dashboard-Btn">
                        <g id="Add-Candidate" transform="translate(35 12)">
                            <g id="💚-Icon" transform="translate(0 1)">
                                <mask id="mask-2-back" fill="#fff">
                                    <use xlinkHref="#path-1" />
                                </mask>
                                <use id="Mask" fill="#4767A0" xlinkHref="#path-1" />
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export default SVGComponent;