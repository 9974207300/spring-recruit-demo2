import React from 'react';

const SVGComponent = () => (
    <svg width="30" height="30" viewBox="0 0 30 30" xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink">
        <defs>
            <path d="M5.1277704,-2.07168062e-16 L24.8722296,2.07168062e-16 C26.6552671,-1.2037061e-16 27.3018396,0.185651222 27.9536914,0.534265408 C28.6055433,0.882879593 29.1171204,1.39445674 29.4657346,2.04630859 C29.8143488,2.69816044 30,3.34473292 30,5.1277704 L30,24.8722296 C30,26.6552671 29.8143488,27.3018396 29.4657346,27.9536914 C29.1171204,28.6055433 28.6055433,29.1171204 27.9536914,29.4657346 C27.3018396,29.8143488 26.6552671,30 24.8722296,30 L5.1277704,30 C3.34473292,30 2.69816044,29.8143488 2.04630859,29.4657346 C1.39445674,29.1171204 0.882879593,28.6055433 0.534265408,27.9536914 C0.185651222,27.3018396 8.02470732e-17,26.6552671 -1.38112041e-16,24.8722296 L1.38112041e-16,5.1277704 C-8.02470732e-17,3.34473292 0.185651222,2.69816044 0.534265408,2.04630859 C0.882879593,1.39445674 1.39445674,0.882879593 2.04630859,0.534265408 C2.69816044,0.185651222 3.34473292,1.2037061e-16 5.1277704,-2.07168062e-16 Z"
                id="path-1" />
            <path d="M14,14 L14,9 C14,8.44771525 14.4477153,8 15,8 C15.5522847,8 16,8.44771525 16,9 L16,14 L21,14 C21.5522847,14 22,14.4477153 22,15 C22,15.5522847 21.5522847,16 21,16 L16,16 L16,21 C16,21.5522847 15.5522847,22 15,22 C14.4477153,22 14,21.5522847 14,21 L14,16 L9,16 C8.44771525,16 8,15.5522847 8,15 C8,14.4477153 8.44771525,14 9,14 L14,14 Z"
                id="path-3" />
        </defs>
        <g id="Product-Flow" fill="none" fillRule="evenodd">
            <g id="Openings" transform="translate(-1078 -224)">
                <g id="Job-Openings" transform="translate(40 200)">
                    <g id="Opening1">
                        <g id="Coordinator" transform="translate(983 24)">
                            <g id="Add-Coordinator" transform="translate(55)">
                                <g id="Avatar">
                                    <mask id="mask-2-opening-add-coordinator" fill="#fff">
                                        <use xlinkHref="#path-1" />
                                    </mask>
                                    <use id="Rectangle-5" fillOpacity="0" fill="#051033" xlinkHref="#path-1"
                                    />
                                    <g id="🎨-Color" mask="url(#mask-2-opening-add-coordinator)" fill="#4767A0">
                                        <polygon id="Gradient-/-2" points="0 0 30 0 30 30 0 30" />
                                    </g>
                                    <mask id="mask-4" fill="#fff">
                                        <use xlinkHref="#path-3" />
                                    </mask>
                                    <use id="Mask" fill="#FFF" fillRule="nonzero" xlinkHref="#path-3" />
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export default SVGComponent;