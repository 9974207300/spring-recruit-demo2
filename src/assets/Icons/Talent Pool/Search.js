import React from 'react';

const SVGComponent = () => (
  <svg width="13" height="13" viewBox="0 0 13 13" xmlns="http://www.w3.org/2000/svg"
xmlnsXlink="http://www.w3.org/1999/xlink">
    <defs>
        <path d="M6.16666667,10.3333333 C8.46785312,10.3333333 10.3333333,8.46785312 10.3333333,6.16666667 C10.3333333,3.86548021 8.46785312,2 6.16666667,2 C3.86548021,2 2,3.86548021 2,6.16666667 C2,8.46785312 3.86548021,10.3333333 6.16666667,10.3333333 Z M10.8386539,9.66014258 L12.5892557,11.4107443 C12.9146926,11.7361813 12.9146926,12.2638187 12.5892557,12.5892557 C12.2638187,12.9146926 11.7361813,12.9146926 11.4107443,12.5892557 L9.66014258,10.8386539 C8.68632547,11.567997 7.47694592,12 6.16666667,12 C2.94500563,12 0.333333333,9.38832771 0.333333333,6.16666667 C0.333333333,2.94500563 2.94500563,0.333333333 6.16666667,0.333333333 C9.38832771,0.333333333 12,2.94500563 12,6.16666667 C12,7.47694592 11.567997,8.68632547 10.8386539,9.66014258 Z"
        id="path-1" />
    </defs>
    <g id="Product-Flow" fill="none" fillRule="evenodd">
        <g id="Talent-Pool" transform="translate(-861 -121)">
            <g id="Search" transform="translate(366 106)">
                <g id="Icon" transform="translate(495 15)">
                    <mask id="mask-2-tp-search" fill="#fff">
                        <use xlinkHref="#path-1" />
                    </mask>
                    <use id="Mask" fillOpacity="0" fill="#051033" fillRule="nonzero" xlinkHref="#path-1"
                    />
                    <g id="Icon-Color" mask="url(#mask-2-tp-search)" fill="#182C4F">
                        <polygon points="0 0 20 0 20 20 0 20" transform="translate(-3 -3)" id="Black-/-Black-800"
                        />
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
);

export default SVGComponent;