import React from 'react';

const SVGComponent = () => (
    <svg width="34" height="34" viewBox="0 0 34 34" xmlns="http://www.w3.org/2000/svg">
        <g id="Product-Flow" fill="none" fillRule="evenodd">
            <g id="Todo---Full-View-for-Interviewer" transform="translate(-1083 -251)">
                <g id="Todo1" transform="translate(365 200)">
                    <g transform="translate(719)" id="Action-Icons">
                        <g id="Message-Btn" transform="translate(0 52)">
                            <g id="Message-Icon">
                                <circle id="Oval" stroke="#4767A0" strokeWidth="1.6" fill="#FFF" cx="16"
                                    cy="16" r="16" />
                                <path d="M24.000085,11.187592 C24.0001985,11.1953779 24.0001983,11.2031607 24.000085,11.2109379 L24.000085,21.2 C24.000085,21.8627417 23.4628267,22.4 22.800085,22.4 L9.20008502,22.4 C8.53734332,22.4 8.00008502,21.8627417 8.00008502,21.2 L8.00008502,11.2109379 C7.99997175,11.2031607 7.99997157,11.1953779 8.00008502,11.187592 L8.00008502,10.8 C8.00008502,10.1372583 8.53734332,9.6 9.20008502,9.6 L22.800085,9.6 C23.4628267,9.6 24.000085,10.1372583 24.000085,10.8 L24.000085,11.187592 Z M10.2423055,11.2 L16.000085,15.0385197 L21.7578645,11.2 L10.2423055,11.2 Z M22.400085,12.6948137 L16.4438452,16.6656402 C16.1751256,16.8447866 15.8250444,16.8447866 15.5563249,16.6656402 L9.60008502,12.6948137 L9.60008502,20.8 L22.400085,20.8 L22.400085,12.6948137 Z"
                                    id="Mask" fill="#4767A0" fillRule="nonzero" />
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export default SVGComponent;