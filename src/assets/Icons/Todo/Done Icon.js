import React from 'react';

const SVGComponent = () => (
    <svg width="34" height="34" viewBox="0 0 34 34" xmlns="http://www.w3.org/2000/svg">
        <g id="Product-Flow" fill="none" fillRule="evenodd">
            <g id="Todo---Full-View-for-Interviewer" transform="translate(-1083 -199)"
                fill="#FFF" stroke="#4767A0" strokeWidth="1.6">
                <g id="Todo1" transform="translate(365 200)">
                    <g transform="translate(719)" id="Action-Icons">
                        <g id="Mark-Done-Btn">
                            <path d="M16,32 C7.163444,32 0,24.836556 0,16 C0,7.163444 7.163444,0 16,0 C24.836556,0 32,7.163444 32,16 C32,24.836556 24.836556,32 16,32 Z M21.3026283,10.8686292 L12.8339991,19.3372583 L9.93137085,16.43463 C9.30653198,15.8097911 8.29346802,15.8097911 7.66862915,16.43463 C7.04379028,17.0594689 7.04379028,18.0725328 7.66862915,18.6973717 L11.7026283,22.7313708 C12.3274672,23.3562097 13.3405311,23.3562097 13.96537,22.7313708 L23.56537,13.1313708 C24.1902089,12.506532 24.1902089,11.493468 23.56537,10.8686292 C22.9405311,10.2437903 21.9274672,10.2437903 21.3026283,10.8686292 Z"
                                id="Done-Icon" />
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export default SVGComponent;