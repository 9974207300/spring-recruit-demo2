import React from 'react';

const SVGComponent = () => (
    <svg width="18" height="17" viewBox="0 0 18 17" xmlns="http://www.w3.org/2000/svg">
        <g id="Product-Flow" fill="none" fillRule="evenodd">
            <g id="Candidate-Profile---Feedback-section-+-Multiple-company-feedback"
                transform="translate(-695 -369)" fill="#27628C" stroke="#4767A0" strokeWidth="1.2">
                <g id="Company-Feedback1" transform="translate(326 221)">
                    <g id="Feedback1" transform="translate(60 91.512)">
                        <g id="Feedback" transform="translate(60 54.335)">
                            <g id="Rating">
                                <g id="criteria4">
                                    <g id="Rating" transform="translate(187 3.733)">
                                        <polygon id="Star-Copy-3" points="71 11.2 66.297718 13.5073269 67.1957739 8.62033011 63.3915479 5.15933978 68.648859 4.44633655 71 0 73.351141 4.44633655 78.6084521 5.15933978 74.8042261 8.62033011 75.702282 13.5073269"
                                        />
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export default SVGComponent;