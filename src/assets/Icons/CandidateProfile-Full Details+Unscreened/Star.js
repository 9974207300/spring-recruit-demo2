import React from 'react';

const SVGComponent = () => (
    <svg width="18" height="18" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg">
        <g id="Product-Flow" fill="none" fillRule="evenodd">
            <g id="Candidate-Profile---Full-Details-+-Screened-+-Interview-Not-scheduled"
                transform="translate(-1184 -297)" stroke="#4767A0" strokeWidth="1.2">
                <g id="Candidate-Profile-Details" transform="translate(326 202)">
                    <g id="Profile-Details">
                        <g id="Details-View" transform="translate(40 92)">
                            <polygon id="Star" points="827 17 822.297718 19.472136 823.195774 14.236068 819.391548 10.527864 824.648859 9.76393202 827 5 829.351141 9.76393202 834.608452 10.527864 830.804226 14.236068 831.702282 19.472136"
                            />
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export default SVGComponent;