import actions from './actions';

const initState = { idToken: null, user: null };

export default function authReducer(state = initState, action) {
  switch (action.type) {
    case actions.LOGIN_SUCCESS:
      return { ...state, idToken: action.token };
    case actions.LOGOUT:
      return initState;
    case actions.SET_USER:
      return { ...state, user: action.user }
    default:
      return state;
  }
}
