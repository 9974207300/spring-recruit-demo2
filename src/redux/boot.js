import { store } from './store';
import authActions from './auth/actions';

export default () =>
  new Promise(async (res, rej) => {
    try {
      let token = localStorage.getItem('id_token');

      if (token) {
        store.dispatch(authActions.login(token));
      }
      res();
    } catch (err) {
      localStorage.removeItem('id_token');
      rej(err);
    }
  });