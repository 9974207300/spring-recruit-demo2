import React, { Component } from 'react';
import Router from './router';

export default class Todo extends Component {
  render() {
    const { url } = this.props.match;

    return (
      <div>
          <Router url={url} />
      </div>
    );
  }
}
