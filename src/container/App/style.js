import styled from 'styled-components';
import { Layout } from 'antd';

const { Header } = Layout;

export const Logo = styled.div`
  width: 120px;
  height: 45px;
  background-image: url('/logo.png');
  margin: 8px 0 auto 40px;
  background-image: no-repeat;
`;

export const Topbar = styled(Header)`
    padding: 0;
    background-color: #fff;
    display: flex;
    flex-grow: 1;
    justify-content: space-between;
    border-bottom: 1px solid rgba(0,0,0,0.09);
    box-shadow: 0 3px 0px -2px #eee;
    position: relative;
    border-radius: 10px;
    line-height: 56px;
    height: 56px;
    width: 1280px;
    margin: 0 auto;

`;

export const Options = styled.div`
  display: flex;
  justify-content: end;
  width: 120px;
  margin: auto 60px;
`;