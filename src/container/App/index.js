import React, { Component } from 'react';
import { connect } from 'react-redux';
import AppRouter from './router';
import { Layout, Menu, Icon, Avatar } from 'antd';
import { Topbar, Logo, Options } from './style';
import NotificationIcon from '../../assets/Icons/Extras/Notification';

const { Content } = Layout;

export class App extends Component {

  constructor(props){
    super(props)

    const route = props.location.pathname.split('/')[2];

    this.state = {
      selectedKey: route
    }
  }

  changeTab = (e) => {
    this.setState({ selectedKey: e.key });
    this.props.history.push(e.key);
  }

  render() {
    const { url } = this.props.match;

    return (
      <div>

        <Layout className="layout" style={{ backgroundColor: '#fff' }}>
          <Topbar>
            <Logo />
            <Menu
              theme="light"
              mode="horizontal"
              defaultSelectedKeys={[this.state.selectedKey]}
              style={{ lineHeight: '50px', color: '#8990a5', fontFamily: 'sanFranciscoMedium' }}
              onSelect={this.changeTab}
            >
              <Menu.Item key="dashboard">Dashboard</Menu.Item>
              <Menu.Item key="openings">Openings</Menu.Item>
              <Menu.Item key="todo">Todo List</Menu.Item>
              <Menu.Item key="talent-pool">Talent Pool</Menu.Item>
            </Menu>
            <Options>
              <Icon style={{ marginRight: '10px', fontSize: '20px' }} component={NotificationIcon} />
              <Avatar style={{ marginRight: '10px', backgroundColor: '#4b5ba0', height: '23px', width: '23px', lineHeight: '22px' }}>JR</Avatar>
              <Icon style={{ marginRight: '10px', color: '#4b5ba0', fontSize: '20px' }} type="down" />
            </Options>
          </Topbar>
          <Content style={{ padding: '49px 40px', backgroundColor: '#fff' }}>
            <AppRouter url={url} />
          </Content>
        </Layout>

      </div>
    );
  }
}

export default connect(
  state => ({
    auth: state.Auth
  }),
  {}
)(App);
