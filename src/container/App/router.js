import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import Openings from '../Modules/Openings';
import TalentPool from '../Modules/TalentPool';
import Dashboard from '../Modules/Dashboard';
import Todo from '../Modules/Todo';

const routes = [
  {
    path: 'openings',
    component: Openings,
  },
  {
    path: 'dashboard',
    component: Dashboard,
  },
  {
    path: 'todo',
    component: Todo,
  },
  {
    path: 'talent-pool',
    component: TalentPool,
  },
];

class AppRouter extends Component {
  render() {
    const { url, style } = this.props;
    return (
      <div style={style}>
        {routes.map(singleRoute => {
          const { path, exact, ...otherProps } = singleRoute;
          return (
            <Route
              exact={exact === false ? false : true}
              key={singleRoute.path}
              path={`${url}/${singleRoute.path}`}
              {...otherProps}
            />
          );
        })}
      </div>
    );
  }
}

export default AppRouter;
