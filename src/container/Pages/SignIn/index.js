import React from 'react';
import {
    SignupWrapper, ContentWrapper, FormWrapper, LogoWrapper,
    RecruitWrapper, SpringWrapper, ButtonWrapper
} from './style';
import './style.css';

import { Button, Input, Icon } from 'antd';
import EmailIcon from '../../../assets/Icons/Auth/Email';
import PasswordIcon from '../../../assets/Icons/Auth/Password';

export default class SignIn extends React.Component {
    state = {
        email: '',
        password: ''
    };

    handleChange = ({ target }) => {
        this.setState({ [target.name]: target.value });
    }

    render() {
        return (
            <SignupWrapper>
                <LogoWrapper>
                    <SpringWrapper>SPRING</SpringWrapper>
                    <RecruitWrapper>RECRUIT</RecruitWrapper>
                </LogoWrapper>
                <ContentWrapper>
                    <div className="header-text">Log In</div>
                    <div className="already-have-an-acco">
                        Don't have an account?
                        <button
                            className="have-account"
                            onClick={() => this.props.history.push('/signup')}
                        >
                            Sign Up
                        </button>
                    </div>
                    <FormWrapper>
                        <Input
                            className="background-colour center-input"
                            placeholder="Enter Your Email"
                            suffix={<Icon component={EmailIcon} />}
                            name="email"
                            value={this.state.email}
                            onChange={this.handleChange}
                        />

                        <Input
                            className="background-colour"
                            placeholder="Enter Your Password"
                            suffix={<Icon component={PasswordIcon} />}
                            name="password"
                            type="password"
                            value={this.state.password}
                            onChange={this.handleChange}
                        />
                    </FormWrapper>
                    <ButtonWrapper>
                        <h5 className="forgot-password">
                            Forgot Password?
                        </h5>
                        <Button
                            className="login-button"
                            size="large"
                        >
                            Log In
                    </Button>
                    </ButtonWrapper>
                </ContentWrapper>
            </SignupWrapper>
        )
    }
}   