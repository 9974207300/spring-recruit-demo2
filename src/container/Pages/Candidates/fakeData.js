module.exports = {
    stages: [
        'SCREENING',
        'TELEPHONIC',
        'F2F INTERVIEW',
        'MAKE OFFER',
        'SELECTED',
    ],
    candidates: [
        {
            id: '1',
            name: 'Brijesh Agarwal',
            details: 'Lead Designer, Solid Designs, Bangalore',
            stage: 'Screening',
            appliedDate: '10th March, 2019'
        },
        {
            id: '2',
            name: 'Brijesh Agarwal',
            details: 'Lead Designer, Solid Designs, Bangalore',
            stage: 'Screening',
            appliedDate: '10th March, 2019'
        },
        {
            id: '3',
            name: 'Brijesh Agarwal',
            details: 'Lead Designer, Solid Designs, Bangalore',
            stage: 'Screening',
            appliedDate: '10th March, 2019'
        },
        {
            id: '4',
            name: 'Brijesh Agarwal',
            details: 'Lead Designer, Solid Designs, Bangalore',
            stage: 'Screening',
            appliedDate: '10th March, 2019'
        }
    ]
};