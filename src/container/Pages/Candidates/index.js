import React from 'react';
import './style.css';
import {
    HeaderContainer, FilterContainer, FilterWrapper, ListContainer,
    ListFilterWrapper, CandidateHeaderWrapper, CandidateListWrapper,
    ListCheckboxWrapper, CandidateWrapper, DetailWrapper, StageWrapper, DateWrapper,
    FilterListWrapper
} from './style';
import { Button, Typography, Icon, Tag, Popover, Menu, Checkbox } from 'antd';
import AddIcon from '../../../assets/Icons/Openings/Add Coordinator';

import fakeData from './fakeData';

const { Title } = Typography;

export default class CandidatesList extends React.Component {
    state = {
        ...fakeData,
        sortOptions: [
            'Applied Date',
            'Name',
            'Current Stage'
        ],
        selectedSortOption: 'Applied Date',
        selectedCandidates: []
    };

    candidateSelected = ({ target }) => {
        let { selectedCandidates } = this.state;
        if (target.checked) {
            selectedCandidates.push(target.id);
        } else {
            selectedCandidates = selectedCandidates.filter(x => x !== target.id);
        }

        this.setState({ selectedCandidates });
    }

    selectAllCandidates = ({ target }) => {
        let { selectedCandidates, candidates } = this.state;
        if (target.checked) {
            selectedCandidates = candidates.map(x => x.id);
        } else {
            selectedCandidates = [];
        }

        this.setState({ selectedCandidates });
    }

    render() {
        const isAllSelected = this.state.selectedCandidates.length === this.state.candidates.length;
        return (
            <div>
                <HeaderContainer>
                    <div style={{ flexGrow: '40' }}>
                        <Title level={3} style={{ color: '#4b5ba0', fontFamily: 'sanFranciscoMedium', fontWeight: '500', marginBottom: 0 }}>
                            Senior Product Desinger
                            <Tag style={{ marginLeft: '10px', lineHeight: '14px', fontSize: ' 10px' }} color="#adb6c4">View Job Details</Tag>
                        </Title>
                        <Title level={4} style={{ color: '#4b5ba0', marginTop: 0, lineHeight: 1, fontWeight: 500, fontSize: '15px' }}>
                            CANDIDATE LIST
                        </Title>
                    </div>
                    <div>
                        <Button ghost size="large" type="primary">
                            <Icon component={AddIcon} />
                            Add Candidate
                        </Button>
                    </div>
                </HeaderContainer>

                <div style={{ display: 'flex' }}>
                    <FilterContainer>
                        <Title level={4} style={{ fontFamily: 'sanFranciscoMedium', color: '#545e7c', marginTop: 0, lineHeight: 1.5, fontWeight: 500, fontSize: '15px' }}>
                            FILTERS:
                        </Title>
                        <FilterWrapper>
                            <Title level={4} style={{ marginLeft: '20px', fontFamily: 'sanFranciscoMedium', color: '#545e7c', lineHeight: 1, fontWeight: 500, fontSize: '15px' }}>
                                CANDIDATES
                            </Title>

                            <FilterListWrapper>
                                <label className="filter-container">
                                    <input type="checkbox" />
                                    <span className="checkmark2"></span>
                                </label>

                                <span>All Candidates</span>
                                <span style={{ margin: 'auto 0 auto auto' }}>11</span>
                            </FilterListWrapper>

                            <FilterListWrapper>
                                <label className="filter-container">
                                    <input type="checkbox" />
                                    <span className="checkmark2"></span>
                                </label>

                                <span>Stared candidates</span>
                                <span style={{ margin: 'auto 0 auto auto' }}>11</span>
                            </FilterListWrapper>

                            <FilterListWrapper>
                                <label className="filter-container">
                                    <input type="checkbox" />
                                    <span className="checkmark2"></span>
                                </label>

                                <span>Added by me</span>
                                <span style={{ margin: 'auto 0 auto auto' }}>11</span>
                            </FilterListWrapper>

                            <FilterListWrapper>
                                <label className="filter-container">
                                    <input type="checkbox" />
                                    <span className="checkmark2"></span>
                                </label>

                                <span>Archived</span>
                                <span style={{ margin: 'auto 0 auto auto' }}>11</span>
                            </FilterListWrapper>
                        </FilterWrapper>
                    </FilterContainer>

                    <ListContainer>
                        <ListFilterWrapper>
                            <div>
                                <Popover
                                    placement="bottom"
                                    trigger="click" content={
                                        <Menu
                                            style={{ borderRight: 0 }}
                                        // onSelect={this.changeFilter}
                                        >
                                            {
                                                this.state.stages.map((option, index) => (
                                                    <Menu.Item
                                                        key={index}
                                                        className="stage-filter"
                                                    >
                                                        <span style={{ color: '#545e7c' }}>
                                                            {option}
                                                        </span>
                                                    </Menu.Item>
                                                ))
                                            }
                                        </Menu>
                                    }>
                                    <Button size="small" style={{ fontSize: '13px', marginRight: '10px', width: '150px', backgroundColor: '#f1f1f4', border: 'none', color: '#8990a5', padding: '0 16px' }}>
                                        Move To Stage
                                    <Icon type="down" />
                                    </Button>
                                </Popover>

                                <Button size="small" style={{ fontSize: '13px', width: '150px', backgroundColor: '#f1f1f4', border: 'none', color: '#8990a5', padding: '0 16px' }}>
                                    Archive
                                </Button>
                            </div>

                            <div>
                                <Popover
                                    placement="bottom"
                                    style={{ width: '250px' }}
                                    content={
                                        <Menu
                                            style={{ borderRight: 0 }}
                                            onSelect={({ key }) => this.setState({ selectedSortOption: key })}
                                        >
                                            {
                                                this.state.sortOptions.map((option) => (
                                                    <Menu.Item
                                                        key={option}
                                                        className="stage-filter"
                                                    >
                                                        <span style={{ color: '#545e7c' }}>
                                                            {option}
                                                        </span>
                                                    </Menu.Item>
                                                ))
                                            }
                                        </Menu>
                                    }>
                                    <Button size="small" className="sort-filter">
                                        Sort By: <span className="sort-filter-name">{this.state.selectedSortOption}</span>
                                        <Icon type="down" className="filter-icon" />
                                    </Button>
                                </Popover>
                            </div>
                        </ListFilterWrapper>

                        <div style={{ marginTop: '10px' }}>
                            <CandidateHeaderWrapper>
                                <ListCheckboxWrapper>
                                    <label className="container">
                                        <input type="checkbox" checked={isAllSelected} onChange={this.selectAllCandidates} />
                                        <span className="checkmark"></span>
                                    </label>
                                </ListCheckboxWrapper>
                                <CandidateWrapper>Candidates</CandidateWrapper>
                                <DetailWrapper>Details</DetailWrapper>
                                <StageWrapper>Curent Stage</StageWrapper>
                                <DateWrapper>Applied Date</DateWrapper>
                            </CandidateHeaderWrapper>

                            {
                                this.state.candidates.map(can => (
                                    <CandidateListWrapper key={can.id}>
                                        <ListCheckboxWrapper>
                                            <label className="container">
                                                <input type="checkbox" id={can.id} onChange={this.candidateSelected} checked={this.state.selectedCandidates.includes(can.id)} />
                                                <span className="checkmark"></span>
                                            </label>
                                        </ListCheckboxWrapper>
                                        <CandidateWrapper>{can.name}</CandidateWrapper>
                                        <DetailWrapper>{can.details}</DetailWrapper>
                                        <StageWrapper>{can.stage}</StageWrapper>
                                        <DateWrapper>{can.appliedDate}</DateWrapper>
                                    </CandidateListWrapper>
                                ))
                            }
                        </div>
                    </ListContainer>
                </div>
            </div>
        )
    }
}
