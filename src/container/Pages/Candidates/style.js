import styled from 'styled-components';

export const HeaderContainer = styled.div`
    display: flex;
    justify-content: space-between;
    margin: 15px auto 53px;
    height: 40px;
    width: 1200px;
`;

export const ModalWrapper = styled.div`
    display: flex;
    flex-direction: column;
    text-align: center;
`;

export const ModalElementWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    margin-top: 25px;
    text-align: left;
`;

export const FilterWrapper = styled.div`
    height: ${window.innerHeight - 220}px;
    border: 1px solid #eff0f4;
    border-radius: 5px;
    box-shadow: 0px 0px 8px 0px #eee;
    display: flex;
    flex-direction: column;
    padding-top: 20px;
`;

export const FilterContainer = styled.div`
    width: 25%;
    height: ${window.innerHeight - 200}px;
    margin-right: 3%;
`;

export const ListContainer = styled.div`
    width: 72%;
    font-family: sanFranciscoMedium;
`;

export const ListFilterWrapper = styled.div`
    display: flex;
    justify-content: space-between;
`;

export const CandidateHeaderWrapper = styled.div`
    height: 40px;
    line-height: 40px;
    border: 1px solid #eff0f4;
    border-radius: 5px;
    box-shadow: 0px 0px 8px 2px #eee;
    display: flex;
    padding: 0px 30px;
    justify-content: space-between;
    background-color: #eff0f4;
    font-size: 12px;
    color: #697398;
    margin-bottom: 20px;
`;

export const ListCheckboxWrapper = styled.div`
    width: 7%;
`;

export const CandidateWrapper = styled.div`
    width: 17%;
`;

export const DetailWrapper = styled.div`
    width: 42%;
`;

export const StageWrapper = styled.div`
    width: 20%;
`;

export const DateWrapper = styled.div`
    width: 14%;
`;

export const CandidateListWrapper = styled.div`
    height: 35px;
    line-height: 35px;
    border-radius: 5px;
    box-shadow: 0px 0px 8px 2px #eee;
    display: flex;
    padding: 0px 30px;
    justify-content: space-between;
    font-size: 12px;
    color: #697398;
    margin-bottom: 20px;
    font-weight: 500;
`;

export const FilterListWrapper = styled.div`
    display: flex;
    padding: 0 20px;
    color: #8990a5;
    font-family: sanFranciscoMedium;
    line-height: 2;

    :hover {
        background-color: #f4f5fa;
    }
`;