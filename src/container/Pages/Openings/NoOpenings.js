import React from 'react';
import { EmptyWrapper } from './style';
import './style.css';
import { Typography, Icon } from 'antd';
import AddJobIcon from '../../../assets/Icons/Openings/Add Job Opening Btn';


const { Title } = Typography;

export default class NoOpening extends React.Component {
    state = {};

    render() {
        return (
            <EmptyWrapper>
                    <Title level={2} className="empty-opening-wrapper">
                        Get started by adding a Job opening!
                    </Title>
                    <Icon component={AddJobIcon} style={{margin: '0 auto auto'}} />
            </EmptyWrapper>
        )
    }
}