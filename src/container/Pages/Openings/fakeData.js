module.exports = {
    openings: [
        {
            title: 'UI / UX Designer',
            description: 'Design Studios, Bangalore, IN',
            coordinator: 'BR',
            stages: [
                {
                    stage: 'SCREENING',
                    candidates: 4
                },
                {
                    stage: 'TELEPHONIC',
                    candidates: 4
                },
                {
                    stage: 'F2F INTERVIEW',
                    candidates: 3
                },
                {
                    stage: 'MAKE OFFER',
                    candidates: 4
                },
                {
                    stage: 'SELECTED',
                    candidates: 4
                }
            ]
        },
        {
            title: 'Project Manager',
            description: 'Design Studios, Bangalore, IN',
            coordinator: 'SP',
            stages: [
                {
                    stage: 'SCREENING',
                    candidates: 4
                },
                {
                    stage: 'TELEPHONIC',
                    candidates: 2
                },
                {
                    stage: 'F2F INTERVIEW',
                    candidates: 4
                },
                {
                    stage: 'MAKE OFFER',
                    candidates: 1
                }
            ]
        }
    ],
    coordinators: [
        'Judy Rivera',
        'Brijesh Agarwal',
        'Dipesh Agarwal',
        'Sandal Jalan'
    ]
};

