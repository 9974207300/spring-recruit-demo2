import React from 'react';
import { OpeningCardWrapper } from './style';
import { Typography, Icon, Avatar, Popover, Input, Button } from 'antd';
import './style.css';
import EditIcon from '../../../assets/Icons/Openings/Edit Icon';
import AddCoordinatorIcon from '../../../assets/Icons/Openings/Add Coordinator';

import getAvatar from '../../../services/getAvatar';

const { Title } = Typography;
const { Search } = Input;

export default class NoOpening extends React.Component {
    state = {
        openings: this.props.openings,
        coordinators: this.props.coordinators,
        coordinatorText: ''
    };

    handleCoordinatorPopover = index => visible => {
        const { openings } = this.state;
        openings[index].popoverVisible = visible;
        this.setState({ openings, coordinatorText: '' });
    }

    isCoordinatorValid = () => {
        const valid = this.state.coordinatorText ? this.state.coordinators
            .filter(x => x.toLowerCase().includes(this.state.coordinatorText.toLowerCase()))
            .length : true;

        const emailRegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        const isEmail = emailRegExp.test(this.state.coordinatorText);
        return { valid, isEmail };
    }

    render() {
        return (
            <div className="card-container">
                {
                    this.state.openings.map((data, index) => (
                        <OpeningCardWrapper key={index}>
                            <div>
                                <div className="card-header-text">
                                    {data.title}
                                </div>
                                <div className="card-subtitle-text">
                                    {data.description}
                                </div>
                            </div>
                            <div className="display-flex">
                                {
                                    data.stages.map((stage, i) => {
                                        let isLast = i === (data.stages.length - 1);
                                        return <div key={i} className="stage-list" style={{ borderRight: !isLast ? '1px solid #eee' : '' }}>
                                            <div className="stage-list-data">
                                                <div className="card-candidate-count">{stage.candidates}</div>
                                                <span className="card-stage-name">{stage.stage}</span>
                                            </div>
                                        </div>
                                    })
                                }
                            </div>
                            <div className="btn-list">
                                <div className="margin-right-30">
                                    <div className="coordinator-list">
                                        <Avatar shape="square" className="coordinator-avatar">{getAvatar(data.coordinator)}</Avatar>
                                        <Popover placement="bottomLeft"
                                            content={
                                                <div className="coordinator-popover">
                                                    <Search
                                                        placeholder="Search name or email"
                                                        // onSearch={value => console.log({ value })}
                                                        onChange={e => this.setState({ coordinatorText: e.target.value })}
                                                        className="coordinator-popover-search"
                                                        value={this.state.coordinatorText}
                                                    />

                                                    {
                                                        (this.isCoordinatorValid().valid && this.state.coordinatorText)
                                                            ? <div className="coordinator-list-result">
                                                                {
                                                                    this.state.coordinators
                                                                        .filter(x => x.toLowerCase().includes(this.state.coordinatorText.toLowerCase()))
                                                                        .map((c, ind) => (
                                                                            <div key={ind} style={{ display: 'flex' }}>
                                                                                <Avatar shape="square" className="coordinator-list-result-avatar">{getAvatar(c)}</Avatar>
                                                                                <div className="coordinator-result-name">{c}</div>
                                                                                <Button ghost size="large" type="primary" className="coordinator-result-btn">
                                                                                    Add
                                                                                </Button>
                                                                            </div>
                                                                        ))
                                                                }
                                                            </div>
                                                            : null
                                                    }

                                                    {
                                                        (!this.isCoordinatorValid().valid)
                                                            ? <div>
                                                                <small style={{ color: '#bfbfbf' }}>No result found</small>
                                                            </div>
                                                            : null
                                                    }

                                                    {
                                                        (!this.isCoordinatorValid().valid && this.isCoordinatorValid().isEmail)
                                                            ? <div style={{ display: 'flex', marginTop: '10px' }}>
                                                                <div style={{margin: 'auto 0'}}>{this.state.coordinatorText}</div>
                                                                <Button ghost size="large" type="primary" style={{ border: 'none', boxShadow: 'none', marginLeft: 'auto' }}>
                                                                    Invite
                                                                </Button>
                                                            </div>
                                                            : null
                                                    }
                                                </div>
                                            }
                                            trigger="click"
                                            visible={data.popoverVisible}
                                            onVisibleChange={this.handleCoordinatorPopover(index)}
                                        >
                                            <Icon component={AddCoordinatorIcon} />
                                        </Popover>
                                    </div>
                                    <h5 style={{ color: '#8b92a6', fontSize: '14px' }}>
                                        Co-ordinator(s)
                                    </h5>
                                </div>
                                <Icon component={EditIcon} style={{ margin: 'auto' }} />
                            </div>
                        </OpeningCardWrapper>
                    ))
                }
            </div>
        )
    }
}