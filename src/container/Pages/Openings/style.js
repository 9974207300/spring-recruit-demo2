import styled from 'styled-components';

export const Title = styled.h4`
    font-weight: bolder;
    color: #697398;
    padding: 16px 16px 0;
    display: flex;
    flex-grow: 1;
    justify-content: space-between !important;

`;

export const HeaderContainer = styled.div`
    display: flex;
    justify-content: space-between;
    margin: 15px auto 53px;
    height: 40px;
    width: 1200px;
`;

export const ModalWrapper = styled.div`
    display: flex;
    flex-direction: column;
    text-align: center;
`;

export const ModalElementWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    margin-top: 25px;
    text-align: left;
`;

export const EmptyWrapper = styled.div`
    width: 1200px;
    height: 500px;
    border: 1px solid #eff0f4;
    border-radius: 8px;
    box-shadow: 0px 0px 8px 0px #eee;
    display: flex;
    flex-direction: column;
`;

export const OpeningCardWrapper = styled.div`
    width: 1200px;
    height: 100px;
    border: 1px solid #eff0f4;
    border-radius: 8px;
    box-shadow: 0px 0px 8px 0px #eee;
    display: flex;
    justify-content: space-between;
    padding: 24px 40px;
    margin: 0 auto 30px;
`;