import React from 'react';
import { HeaderContainer } from './style';
import { Button, Typography, Icon } from 'antd';
import './style.css';
import NoOpening from './NoOpenings';
import OpeningCard from './OpeningCard';
import AddIcon from '../../../assets/Icons/Talent Pool/Add';

import fakeData from './fakeData';

const { Title } = Typography;

export default class Opening extends React.Component {
    state = {
        ...fakeData
    };


    render() {
        return (
            <div>
                <HeaderContainer>
                    <div className="header-container">
                        <Title level={3} className="header-text">
                            All Openings
                        </Title>
                    </div>
                    <div className="header-button-container">
                        <Button ghost size="large" type="primary" className="header-button">
                            <Icon component={AddIcon} />
                            Add Candidate
                        </Button>
                        <Button ghost size="large" type="primary" className="header-button">
                            <Icon component={AddIcon} />
                            Add Opening
                        </Button>
                    </div>
                </HeaderContainer>
                <div>
                    {
                        (this.state.openings.length)
                            ? <OpeningCard openings={this.state.openings} coordinators={this.state.coordinators} />
                            : <NoOpening />
                    }
                </div>
            </div>
        )
    }
}