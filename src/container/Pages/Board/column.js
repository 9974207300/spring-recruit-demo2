import React from 'react';
import { Droppable, Draggable } from 'react-beautiful-dnd';
import { Badge, Icon, Popover, Menu } from 'antd';
import { ColumnContainer, Title, TaskList } from './style';
import './column.css';
import Card from './card';

import FilterIcon from '../../../assets/Icons/Board/Sort by';
import MoveIcon from '../../../assets/Icons/Board/Move Icon';
import EditIcon from '../../../assets/Icons/Board/Edit Icon';

class TaskListComponent extends React.Component {

    render() {
        return (
            <TaskList
                ref={this.props.provided.innerRef}
                {...this.props.provided.droppableProps}>
                {
                    this.props.cards.map((card, i) => <Card card={card} index={i} key={card.id} />)
                }
                {
                    this.props.provided.placeholder
                }
            </TaskList>
        )
    }
}

export default class Column extends React.Component {
    state = {
        showTitleEdit: false,
        filterDatasource: [
            'Interview Date',
            'Name',
            'Total Experience',
            'Added Date',
            'Selectebility %'
        ],
        ...this.props
    }

    componentWillReceiveProps(nextProps) {
        if (
            this.props.cards !== nextProps.cards ||
            this.props.index !== nextProps.index ||
            this.props.task !== nextProps.task
            ) {
                this.setState({ ...nextProps });
            }
    }

    showTitleEdit = val => this.setState({ showTitleEdit: val })

    changeFilter = ({ key }) => {
        this.setState(state => ({
            cards: state.cards.reverse()
        }))
    }

    render() {
        return <Draggable draggableId={this.state.task.id} index={this.state.index}>
            {(provided) => (
                <ColumnContainer
                    className="board-column"
                    {...provided.draggableProps}
                    ref={provided.innerRef}
                >
                    <Title className="column-title-preview">

                        <Badge count={this.state.cards.length} style={{
                            backgroundColor: '#697398', color: '#fff',
                            height: '24px', width: '24px', borderRadius: '50%',
                            padding: 0, lineHeight: '23px', fontSize: '15px', boxShadow: 'none'
                        }} />

                        <div className="modal-preview-title" onMouseOver={() => this.showTitleEdit(true)} onMouseLeave={() => this.showTitleEdit(false)}>
                            <span className="column-title">{this.state.task.title}</span>
                            {
                                (this.state.showTitleEdit) ? <Icon className="column-edit-icon" component={EditIcon} /> : null
                            }
                        </div>
                        <div className="column-items">
                            <Popover
                                title={
                                    <span className="sort-by-popover">
                                        SORT BY:
                                    </span>
                                }
                                arrowPointAtCenter
                                placement="bottomRight"
                                content={
                                    <Menu
                                        className="border-right-0"
                                        onSelect={this.changeFilter}
                                    >
                                        {
                                            this.state.filterDatasource.map((filter, index) => (
                                                <Menu.Item
                                                    key={index}
                                                >
                                                    <span className="sort-by-popover-option">
                                                        {filter}
                                                    </span>
                                                </Menu.Item>
                                            ))
                                        }
                                    </Menu>
                                }
                            >
                                <Icon className="sort-by-icon" component={FilterIcon} />
                            </Popover>
                            <Icon className="column-drag-icon" component={MoveIcon}
                                {...provided.dragHandleProps}
                            />
                        </div>
                    </Title>
                    <Droppable droppableId={this.state.task.id} type="card">
                        {(provided) => (
                            <TaskListComponent
                                cards={this.state.cards}
                                provided={provided}
                            />
                        )}
                    </Droppable>
                </ColumnContainer>
            )}
        </Draggable>
    }
}