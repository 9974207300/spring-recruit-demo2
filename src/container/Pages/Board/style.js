import styled from 'styled-components';

export const CardContainer = styled.div`
    width: 245px;
    margin: 0 auto 16px;
`;

export const ColumnContainer = styled.div`
    width: 285px;
    margin: 8px 16px 8px 0;
    background-color: #e1e0eb;
    padding: 0 8px;
    border-radius: 4px;
    overflow: hidden;
`;
export const TaskList = styled.div`
    padding: 8px;
    padding-right: 20px;
    height: 470px;
    overflow: auto;
    width: 100%;
    box-sizing: content-box;
`;
    
    export const Title = styled.h4`
    width: 245px;
    margin: 0 auto;
    line-height: 20px;
    font-weight: bolder;
    color: #697998;
    padding: 21px 0 11px;
    display: flex;
    flex-grow: 1;
    justify-content: space-between !important;

`;

export const BoardContainer = styled.div`
    display: flex;
    width: 1200px;
    height: 560px;
`;

export const BoardPreviewContainer = styled.div`
    display: flex;
    padding: 5px 12px;
    background-image: linear-gradient(#c7d1eb, #7f92d2);

    & > .board-column {
        width: 100px;
    }

    & .column-title-preview {
        width: 100px;
        padding: 5px 2px;
    }

    & .column-title {
        width: 60px;
        line-height: 10px;
        font-size: 9px;
    }

    & .modal-preview-title {
        line-height: 10px;
        text-align: center;
        width: 60px;
        margin-top: 4px;
    }

    & .anticon {
        margin: 1px;
    }

    & .board-column {
        padding: 0;
    }

    & svg {
        height: 10px;
        width: 10px;
    }
`;

export const HeaderContainer = styled.div`
    display: flex;
    justify-content: space-between;
    margin: 15px auto 53px;
    height: 40px;
    width: 1200px;
`;

export const ModalWrapper = styled.div`
    display: flex;
    flex-direction: column;
    text-align: center;
    height: 394px;
`;

export const ModalElementWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    margin-top: 25px;
    text-align: left;
`;

export const ModalPreviewWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    margin-top: 25px;
    text-align: left;
    flex-direction: column;
    height: 102px;
    overflow: hidden;
    width: 570px;
    padding: 10px 0;
    pointer-events: none;

    & > * {
        font-size: inherit;
    }
    & > h4 {
        padding: 10px 4px;
    }
`;