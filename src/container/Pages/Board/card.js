import React from 'react';
import { Draggable } from 'react-beautiful-dnd';
import { Card, Icon, Avatar, Popover, Menu } from 'antd';
import './card.css';
import { CardContainer } from './style';
import OptionIcon from '../../../assets/Icons/Board/Options';
import StarIcon from '../../../assets/Icons/Board/Star';
import MailIcon from '../../../assets/Icons/Board/Mail';
import DiscardIcon from '../../../assets/Icons/Board/Discard';
import SchedulelIcon from '../../../assets/Icons/Board/Schedule';

const { Meta } = Card;

const Description = () => <div className="meta-cont">
    <Icon className="card-icons" component={StarIcon} />
    <Icon className="card-icons" component={MailIcon} />
    <Icon style={{ margin: 'auto 0 auto 3px' }} component={SchedulelIcon} />
    <Icon className="card-icons" component={DiscardIcon} />
</div>

const Title = (props) => <div className="display-flex">
    <div className="card-avatar">
        <Avatar shape="square" size="large" className="card-avatar-bg">BR</Avatar>
    </div>
    <div className="card-head-cont">
        <div className="card-title-cont">
            <div className="cd-card-title">
                {props.card.title}
            </div>
            <div className="display-flex">
                <Popover
                    trigger="click"
                    placement="bottomLeft"
                    arrowPointAtCenter
                    content={
                        <Menu
                            className="card-option-cont"
                        >
                            <Menu.Item className="card-options">
                                <span className="card-option-color">
                                    Mark Favourite
                                </span>
                            </Menu.Item>
                            <Menu.Item className="card-options">
                                <span className="card-option-color">
                                    Send Message
                                </span>
                            </Menu.Item>
                            <Menu.Item className="card-options">
                                <span className="card-option-color">
                                    Schedule Interview
                                </span>
                            </Menu.Item>
                            <Menu.Item className="card-options">
                                <span className="card-option-color">
                                    Add Feedback
                                </span>
                            </Menu.Item>
                            <Menu.Item className="card-options">
                                <span className="card-option-color">
                                    Edit Details
                                </span>
                            </Menu.Item>
                            <Menu.Item className="card-options">
                                <span className="card-option-color">
                                    Archieve
                                </span>
                            </Menu.Item>
                        </Menu>
                    }
                >
                    <Icon className="card-option-icon" component={OptionIcon} />
                </Popover>
            </div>
        </div>
        <div className="ant-card-meta-description card-subtitle">
            Developer
        </div>
        <Description />
    </div>
</div>

export default class CardComponent extends React.Component {
    render() {
        return <Draggable draggableId={this.props.card.id} index={this.props.index}>
            {(provided) => (
                <CardContainer
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    ref={provided.innerRef}
                >
                    <Card
                        className="candidate-card"
                        title={<Title card={this.props.card} />}
                    >
                        <Meta
                            className="cd-card-meta"
                            title={<small className="candidate-card-meta">Unassigned</small>}
                        />
                    </Card>
                </CardContainer>
            )}
        </Draggable>
    }
}