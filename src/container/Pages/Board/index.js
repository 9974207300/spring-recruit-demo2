import React from 'react';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import { BoardContainer, HeaderContainer, ModalElementWrapper, ModalWrapper, ModalPreviewWrapper, BoardPreviewContainer } from './style';
import { Button, Typography, Icon, Modal, Input, Row, Col, Select, Radio } from 'antd';
import './style.css';
import Column from './column';
import AddIcon from '../../../assets/Icons/Talent Pool/Add';
import SchedulelIcon from '../../../assets/Icons/Board/ScheduleLarge';

import initialData from './initial-data';

const { Title, Text } = Typography;
const { Option } = Select;
const { Group: RadioGroup } = Radio;

export default class Board extends React.Component {
    state = {
        ...initialData,
        stageForm: {
            name: '',
            position: 0,
            hasScheduling: true
        },
        previewTask: [],
        showStageModal: false
    };

    handleCancel = () => {
        this.setState({ previewTask: [], showStageModal: false });
    }

    onDragEnd = async result => {
        const { source, destination, type, draggableId } = result;
        let { stages } = this.state;

        if (!destination) return;

        if (
            source.index === destination.index &&
            source.droppableId === destination.droppableId
        ) return;

        if (type === 'card') {
            stages = stages.map(task => {
                if (task.id === source.droppableId) {
                    task.cards.splice(source.index, 1);
                }

                if (task.id === destination.droppableId) {
                    task.cards.splice(destination.index, 0, draggableId)
                }

                return task;
            });
        } else {
            const sp = stages.splice(source.index, 1);
            stages.splice(destination.index, 0, sp[0]);
        }

        this.setState({ stages });
    }

    addStage = () => {
        const { stages } = this.state;
        const previewTask = [{ id: 'newTask', title: '' }].concat(stages);
        this.setState({ previewTask, showStageModal: true })
    }

    handleChange = name => (event) => {
        const { stageForm, previewTask } = this.state;
        const pos = stageForm.position;
        stageForm[name] = typeof event === 'object' ? event.target.value : event;

        if (name !== 'hasScheduling') {
            previewTask.splice(pos, 1);
            previewTask.splice(stageForm.position, 0, { id: 'newTask', title: stageForm.name });
        }

        this.setState({ stageForm, previewTask });
    }

    render() {
        return (
            <div>
                <HeaderContainer>
                    <div className="header-container">
                        <div className="header-text">
                            UI / UX Designer
                        </div>
                        <div className="header-subtitle">
                            Design Studios, Bangalore, IN
                        </div>
                    </div>
                    <div className="header-button-container">
                        <Button ghost size="large" type="primary">
                            <Icon component={AddIcon} />
                            Add Candidate
                        </Button>
                        <Button ghost size="large" type="primary" onClick={this.addStage}>
                            <Icon component={AddIcon} />
                            Add Stage
                        </Button>
                        <Icon component={SchedulelIcon} />
                    </div>
                </HeaderContainer>
                <DragDropContext
                    onDragEnd={this.onDragEnd}
                >
                    <Droppable droppableId="colums" type="stages" direction="horizontal">
                        {(provided) =>
                            <BoardContainer
                                ref={provided.innerRef}
                                {...provided.droppableProps}
                            >
                                {
                                    this.state.stages.map((task, i) => {
                                        const data = { id: task.id, title: task.title };
                                        const cards = task.cards.map(card => this.state.cards[card]);
                                        return <Column key={task.id} task={data} cards={cards} index={i} />
                                    })
                                }
                                {provided.placeholder}
                            </BoardContainer>
                        }
                    </Droppable>
                </DragDropContext>

                <Modal
                    visible={this.state.showStageModal}
                    closable={false}
                    centered
                    footer={null}
                    bodyStyle={{ padding: '28px 40px 40px', fontSize: '12px' }}
                    width={650}
                    onCancel={this.handleCancel}
                >
                    <ModalWrapper>
                        <div className="modal-as-title">Add Stage</div>
                        <Text type="secondary" className="font-family-mediumn">Define your recruitment flow by adding a stage as per your requirement</Text>

                        <ModalElementWrapper>
                            <Row gutter={16}>
                                <Col xs={12}>
                                    <Text className="modal-ad-title-color">Stage Name</Text>
                                    <Input
                                        size="large"
                                        className="margin-top-5 modal-ad-input"
                                        placeholder="Enter the stage name"
                                        value={this.state.stageForm.name}
                                        onChange={this.handleChange('name')}
                                    />
                                </Col>
                                <Col xs={12}>
                                    <Text className="modal-ad-title-color">Stage Position</Text>
                                    <div>
                                        <Select
                                            disabled={!this.state.stages.length}
                                            size="large"
                                            className="modal-ad-position modal-ad-input"
                                            defaultValue={this.state.stageForm.position}
                                            onChange={this.handleChange('position')}
                                        >
                                            {
                                                this.state.stages.map((option, i) => (
                                                    <Option className="modal-ad-position-option" key={option.id} value={i}>
                                                        <h5>
                                                            Before "{option.title}" Stage
                                                        </h5>
                                                    </Option>
                                                ))
                                            }
                                            {
                                                (this.state.stages.length)
                                                    ? <Option className=",odal-ad-position-option" key="keyOption" value={this.state.stages.length}>
                                                        <h5>
                                                            After "{this.state.stages[this.state.stages.length - 1].title}" Stage
                                                        </h5>
                                                    </Option>
                                                    : null
                                            }
                                        </Select>
                                    </div>
                                </Col>
                            </Row>
                        </ModalElementWrapper>
                        <ModalElementWrapper className="modal-ad-preview">
                            <Row gutter={0} className="w-100">
                                <Col xs={14}>
                                    <Text className="modal-ad-title-color">Does this stage includes scheduling of interview / meetings?</Text>
                                </Col>
                                <Col xs={10} className="modal-ad-radio">
                                    <RadioGroup value={this.state.stageForm.hasScheduling} onChange={this.handleChange('hasScheduling')}>
                                        <Radio value={true}>Yes</Radio>
                                        <Radio value={false}>No</Radio>
                                    </RadioGroup>
                                </Col>
                            </Row>
                        </ModalElementWrapper>

                        <ModalPreviewWrapper>
                            {
                                (this.state.stageForm.name)
                                    ? <div>
                                        <Text className="modal-ad-title-color">Layout Preview</Text>
                                        <DragDropContext
                                            onDragEnd={console.log}
                                        >
                                            <Droppable droppableId="colums" type="tasks" direction="horizontal">
                                                {(provided) =>
                                                    <BoardPreviewContainer
                                                        
                                                        ref={provided.innerRef}
                                                        {...provided.droppableProps}
                                                    >
                                                        {
                                                            this.state.previewTask.map((task, i) => {
                                                                const data = { id: task.id, title: task.title };
                                                                return <Column key={task.id} task={data} cards={[]} index={i} />
                                                            })
                                                        }
                                                        {provided.placeholder}
                                                    </BoardPreviewContainer>
                                                }
                                            </Droppable>
                                        </DragDropContext>
                                    </div>
                                    : null
                            }
                        </ModalPreviewWrapper>

                        <ModalElementWrapper>
                            <Button type="primary" ghost size="large" className="width-120p" onClick={this.handleCancel}>
                                Cancel
                            </Button>
                            <Button type="primary" size="large" className="width-120p">
                                Add
                            </Button>
                        </ModalElementWrapper>
                    </ModalWrapper>
                </Modal>
            </div>
        )
    }
}