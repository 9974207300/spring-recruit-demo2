module.exports = {
    stages: [
        {
            id: 'task1',
            title: 'TELEPHONIC',
            cards: ['card3', 'card4', 'card5', 'card6']
        },
        {
            id: 'task2',
            title: 'TECHNICAL ROUND',
            cards: ['card1', 'card2']
        },
        {
            id: 'task3',
            title: 'SCREENING',
            cards: []
        },
        // {
        //     id: 'task4',
        //     title: 'SCREENING',
        //     cards: []
        // },
        // {
        //     id: 'task5',
        //     title: 'SCREENING',
        //     cards: []
        // }
    ],
    cards: {
        card1: {
            id: 'card1',
            title: 'Brijesh Agarwal'
        },
        card2: {
            id: 'card2',
            title: 'Dipesh Agarwal'
        },
        card3: {
            id: 'card3',
            title: 'Sandal Jalan'
        },
        card4: {
            id: 'card4',
            title: 'Gaurav Jain'
        },
        card5: {
            id: 'card5',
            title: 'Jay Bhatt'
        },
        card6: {
            id: 'card6',
            title: 'Nayan Mittal'
        }
    }
};

