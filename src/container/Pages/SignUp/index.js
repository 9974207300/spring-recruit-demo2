import React from 'react';
import {
    SignupWrapper, ContentWrapper, FormWrapper, LogoWrapper,
    RecruitWrapper, SpringWrapper
} from './style';
import './style.css';
import { Button, Input, Icon } from 'antd';

import URLIcon from '../../../assets/Icons/Auth/URL';
import CompanyNameIcon from '../../../assets/Icons/Auth/Comapny Name';
import EmailIcon from '../../../assets/Icons/Auth/Email';
import PasswordIcon from '../../../assets/Icons/Auth/Password';

export default class SignUp extends React.Component {
    state = {
        email: '',
        companyName: '',
        websiteUrl: '',
        password: ''
    };

    handleChange = ({target}) => {
        this.setState({ [target.name]: target.value });
    }

    render() {
        return (
            <SignupWrapper>
                <LogoWrapper>
                    <SpringWrapper>SPRING</SpringWrapper>
                    <RecruitWrapper>RECRUIT</RecruitWrapper>
                </LogoWrapper>
                <ContentWrapper>
                    <div className="header-text">Sign Up</div>
                    <div className="already-have-an-acco">
                        Already have an account?
                        <button
                            className="have-account"
                            onClick={() => this.props.history.push('/signin')}
                        >
                            Log In
                        </button>
                    </div>
                    <FormWrapper>
                        <Input
                                className="background-colour center-input"
                                placeholder="Enter Your Email"
                                suffix={<Icon component={EmailIcon} />}
                                name="email"
                                value={this.state.email}
                                onChange={this.handleChange}
                            />

                            <Input
                                className="background-colour center-input"
                                placeholder="Enter Your Company Name"
                                suffix={<Icon component={CompanyNameIcon} />}
                                name="companyName"
                                value={this.state.companyName}
                                onChange={this.handleChange}
                            />

                            <Input
                                className="background-colour center-input"
                                placeholder="Enter Your Website URL"
                                suffix={<Icon component={URLIcon} />}
                                name="websiteUrl"
                                value={this.state.websiteUrl}
                                onChange={this.handleChange}
                            />

                            <Input
                                className="background-colour"
                                placeholder="Enter Your Password"
                                suffix={<Icon component={PasswordIcon} />}
                                name="password"
                                type="password"
                                value={this.state.password}
                                onChange={this.handleChange}
                            />
                    </FormWrapper>
                    <Button
                        className="signup-button"
                        size="large"
                    >
                        Sign Up
                    </Button>
                </ContentWrapper>
            </SignupWrapper>
        )
    }
}   