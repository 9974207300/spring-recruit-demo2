import styled from 'styled-components';
import BackgroundSVG from '../../../assets/svg/background.svg';

export const SignupWrapper = styled.div`
    height: ${window.innerHeight}px;
    width: 100%;
    background: url(${BackgroundSVG});
    display: flex;
    justify-content: center;
    flex-direction: column;
`;

export const ContentWrapper = styled.div`
    background-color: #fff;
    padding: 33px 56px;
    text-align: center;
    margin: 57px 286px auto 409px;
    height: 496px;
    width: 464px;
    border-radius: 8px;
`;

export const FormWrapper = styled.div`
    margin: 30px auto;
`;

export const LogoWrapper = styled.div`
    height: 40px;
    width: 103px;
    margin: 55px auto 0 48px;;
`;

export const SpringWrapper = styled.div`
    height: 14.17px;
    width: 54px;
    color: #FFFFFF;
    font-size: 14px;
    line-height: 16px;
    text-align: center;
`;

export const RecruitWrapper = styled.div`
	height: 27px;
	width: 102.89px;
	color: #FFFFFF;
	font-size: 22.4px;
	font-weight: bold;
	line-height: 28px;
	text-align: center;
`;