import React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import { connect } from 'react-redux';

import App from './container/App';
import SignIn from './container/Pages/SignIn';
import SignUp from './container/Pages/SignUp';

const RestrictedRoute = ({ component: Component, isLoggedIn, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      isLoggedIn ? (
        <Component {...props} />
      ) : (
          <Redirect
            to={{
              pathname: '/signin',
              state: { from: props.location }
            }}
          />
        )
    }
  />
);

const PublicRoutes = ({ history, isLoggedIn }) => {
  return (
    <ConnectedRouter history={history}>
      <Switch>
        <Route
          path="/signin"
          exact
          component={SignIn}
        />
        <Route
          path="/signup"
          exact
          component={SignUp}
        />
        <RestrictedRoute
          path="/app"
          component={App}
          isLoggedIn={true}
        />
        <Route path="*" render={() => <Redirect to="/signin" />} />
      </Switch>
    </ConnectedRouter>
  );
};

export default connect(state => ({
  isLoggedIn: state.Auth.idToken !== null
}))(PublicRoutes);
