import React from 'react';
import { Provider } from "react-redux";
import { store, history } from './redux/store';
import PublicRoutes from './router';

export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <PublicRoutes history={history} />
            </Provider>
        )
    }
}