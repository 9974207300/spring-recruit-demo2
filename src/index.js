import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Boot from "./redux/boot";
import './styles.css';

Boot()
  .then(() => {
    ReactDOM.render(<App />, document.getElementById('root'));
  })
  .catch(error => {
    console.error(error);
    ReactDOM.render(<App />, document.getElementById('root'));;
  });
